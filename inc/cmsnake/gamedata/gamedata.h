﻿#ifndef CMSNAKE_GAMEDATA_GAMEDATA_H
#define CMSNAKE_GAMEDATA_GAMEDATA_H

#include "cmsnake/def/gamedata/gamedata.h"
#include "cmsnake/gamedata/stage/stage.h"
#include "cmsnake/gamedata/stage/snake.h"
#include "cmsnake/gamedata/timer.h"
#include "fg/def/core/game/context.h"
#include "fg/def/state/stack.h"
#include "fg/def/window/drawevent.h"
#include "fg/def/controller/raw/event.h"
#include "fg/def/task/manager.h"
#include "fg/def/gl/context.h"
#include "fg/def/common/unique.h"

#include <memory>

namespace cmsnake {
    struct GameData : public fg::UniqueWrapper< GameData >
    {
        fg::GLContext::Unique   glContextUnique;

        fg::StateStack::Unique  stateStackUnique;

        fg::WindowDrawEventRegisterManager::Unique      windowDrawEventRegisterManagerUnique;
        fg::RawControllerEventRegisterManager::Unique   rawControllerEventRegisterManagerUnique;

        Stage       stage;
        TimerData   timerData;

        fg::TaskManager::Unique taskManagerUnique;

        GameData(
            fg::GameContext &
        );

        inline static Unique create(
            fg::GameContext &   _gameContext
        )
        {
            return new GameData( _gameContext );
        }

        static void destroy(
            GameData *
        );

        bool setSnakeDirection(
            const Snake::Direction &
        );
    };
}

#endif  // CMSNAKE_GAMEDATA_GAMEDATA_H
