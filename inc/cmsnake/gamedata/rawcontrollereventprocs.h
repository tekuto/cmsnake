﻿#ifndef CMSNAKE_GAMEDATA_RAWCONTROLLEREVENTPROCS_H
#define CMSNAKE_GAMEDATA_RAWCONTROLLEREVENTPROCS_H

#include "fg/def/controller/raw/event.h"

namespace cmsnake {
    void rawControllerEventProc(
        fg::RawControllerEvent<> &
    );
}

#endif  // CMSNAKE_GAMEDATA_RAWCONTROLLEREVENTPROCS_H
