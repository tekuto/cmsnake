﻿#ifndef CMSNAKE_GAMEDATA_STAGE_SNAKE_H
#define CMSNAKE_GAMEDATA_STAGE_SNAKE_H

#include "cmsnake/def/gamedata/stage/stage.h"

#include <vector>

namespace cmsnake {
    struct Snake
    {
        enum Direction
        {
            NEUTRAL = 0,

            LEFT,
            RIGHT,
            UP,
            DOWN,
        };

        struct Body
        {
            Direction   direction;
            int         length;
        };

        typedef std::vector< Body > Bodies;

        typedef std::vector< int > Foods;

        Direction   direction;
        int         x;
        int         y;

        Bodies  bodies;
        Foods   foods;
    };

    void initialize(
        Snake &
        , const Stage &
    );

    bool setDirection(
        Snake &
        , const Snake::Direction &
    );

    void forward(
        Snake &
    );

    void eat(
        Snake &
    );

    bool isCrashed(
        const Snake &
    );
}

#endif  // CMSNAKE_GAMEDATA_STAGE_SNAKE_H
