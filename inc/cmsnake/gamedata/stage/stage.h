﻿#ifndef CMSNAKE_GAMEDATA_STAGE_STAGE_H
#define CMSNAKE_GAMEDATA_STAGE_STAGE_H

#include "cmsnake/def/gamedata/stage/stage.h"
#include "cmsnake/gamedata/stage/snake.h"

#include <mutex>
#include <random>

namespace cmsnake {
    struct Stage
    {
        std::mutex  mutex;
        Snake       snake;

        std::mt19937                        randomEngine;
        std::uniform_real_distribution<>    distribution;

        int foodX;
        int foodY;
    };

    void initialize(
        Stage &
    );

    int getWidth(
        const Stage &
    );

    int getHeight(
        const Stage &
    );

    int getBlockSize(
        const Stage &
    );

    void getSnake(
        Snake &
        , Stage &
    );

    bool setSnakeDirection(
        Stage &
        , const Snake::Direction &
    );

    void forwardSnake(
        Stage &
    );

    bool isCrashed(
        Stage &
    );
}

#endif  // CMSNAKE_GAMEDATA_STAGE_STAGE_H
