﻿#ifndef CMSNAKE_GAMEDATA_PAINT_H
#define CMSNAKE_GAMEDATA_PAINT_H

#include "cmsnake/def/gamedata/gamedata.h"
#include "fg/def/gl/currentcontext.h"

namespace cmsnake {
    void paint(
        fg::GLCurrentContext &
        , GameData &
    );
}

#endif  // CMSNAKE_GAMEDATA_PAINT_H
