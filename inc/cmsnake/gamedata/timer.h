﻿#ifndef CMSNAKE_GAMEDATA_TIMER_H
#define CMSNAKE_GAMEDATA_TIMER_H

#include <mutex>
#include <condition_variable>
#include <chrono>

namespace cmsnake {
    struct TimerData
    {
        std::mutex              mutex;
        std::condition_variable condition;

        bool                                    started;
        std::chrono::steady_clock::time_point   startTime;
    };

    void initialize(
        TimerData &
    );

    bool timer(
        TimerData &
    );
}

#endif  // CMSNAKE_GAMEDATA_TIMER_H
