﻿#ifndef CMSNAKE_GAMEDATA_WINDOWEVENTPROCS_H
#define CMSNAKE_GAMEDATA_WINDOWEVENTPROCS_H

#include "fg/def/window/drawevent.h"

namespace cmsnake {
    void windowDrawEventProc(
        fg::WindowDrawEvent<> &
    );
}

#endif  // CMSNAKE_GAMEDATA_WINDOWEVENTPROCS_H
