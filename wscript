# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

from waflib.Tools import waf_unit_test

import os.path

APPNAME = 'cmsnake'
VERSION = '0.4.3'

out = 'build'

taf.PACKAGE_NAME = 'cmsnake'

taf.LOAD_TOOLS = [
    'compiler_cxx',
    'waf_unit_test',
    'taf.tools.cpp',
]

cpp.INCLUDES = [
    os.path.join(
        '..',
        'fg',
        'inc',
    ),
]

taf.POST_FUNCTIONS = [
    waf_unit_test.summary,
]
