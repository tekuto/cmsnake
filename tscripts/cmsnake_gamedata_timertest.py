# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'cmsnake-gamedata-timertest'

module.SOURCE = {
    'gamedata' : [
        'timertest.cpp',
        'timer.cpp',
    ],
}
