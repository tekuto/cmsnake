# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.BUILDER = copy.files

module.TARGET = '.'

module.SOURCE = {
    'configs' : [
        'package.fgp',
    ]
}
