# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'cmsnake_files',
    'cmsnake_gamedata_stage_snaketest',
    'cmsnake_gamedata_timertest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'cmsnake'

module.SOURCE = [
    'cmsnake.cpp',
    {
        'gamedata' : [
            'gamedata.cpp',
            {
                'stage' : [
                    'stage.cpp',
                    'snake.cpp',
                ],
            },
            'paint.cpp',
            'timer.cpp',
            'windoweventprocs.cpp',
            'rawcontrollereventprocs.cpp',
        ],
    },
]
