# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'cmsnake-gamedata-stage-snaketest'

module.SOURCE = {
    'gamedata' : {
        'stage' : [
            'snaketest.cpp',
            'snake.cpp',
            'stage.cpp',
        ],
    },
}
