﻿#include "cmsnake/gamedata/paint.h"
#include "cmsnake/gamedata/gamedata.h"
#include "fg/gl/currentcontext.h"
#include "fg/gl/gl.h"

#include <algorithm>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    void translate_(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const int &           _X
        , const int &           _Y
    )
    {
        _glCurrentContext.glTranslatef(
            _X * _BLOCK_SIZE + _BLOCK_SIZE / 2
            , _Y * _BLOCK_SIZE + _BLOCK_SIZE / 2
            , 0
        );
    }

    void rotate_(
        fg::GLCurrentContext &              _glCurrentContext
        , const cmsnake::Snake::Direction & _DIRECTION
    )
    {
        auto    rotationAngle = 0;
        switch( _DIRECTION ) {
        case cmsnake::Snake::Direction::UP:
            rotationAngle = 0;
            break;

        case cmsnake::Snake::Direction::LEFT:
            rotationAngle = 90;
            break;

        case cmsnake::Snake::Direction::DOWN:
            rotationAngle = 180;
            break;

        case cmsnake::Snake::Direction::RIGHT:
            rotationAngle = 270;
            break;

        default:
#ifdef  DEBUG
            std::printf( "E:ありえない値\n" );
#endif  // DEBUG

            return;
            break;
        }

        _glCurrentContext.glRotatef(
            rotationAngle
            , 0
            , 0
            , 1
        );
    }

    void scale_(
        fg::GLCurrentContext &  _glCurrentContext
        , const int &           _SCALE
    )
    {
        _glCurrentContext.glScalef(
            1
            , _SCALE
            , 1
        );
    }

    void translate(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const int &           _X
        , const int &           _Y
    )
    {
        _glCurrentContext.glLoadIdentity();

        translate_(
            _glCurrentContext
            , _BLOCK_SIZE
            , _X
            , _Y
        );
    }

    void rotateTranslate(
        fg::GLCurrentContext &              _glCurrentContext
        , const float &                     _BLOCK_SIZE
        , const cmsnake::Snake::Direction & _DIRECTION
        , const int &                       _X
        , const int &                       _Y
    )
    {
        _glCurrentContext.glLoadIdentity();

        translate_(
            _glCurrentContext
            , _BLOCK_SIZE
            , _X
            , _Y
        );

        rotate_(
            _glCurrentContext
            , _DIRECTION
        );
    }

    void scaleRotateTranslate(
        fg::GLCurrentContext &              _glCurrentContext
        , const float &                     _BLOCK_SIZE
        , const int &                       _SCALE
        , const cmsnake::Snake::Direction & _DIRECTION
        , const int &                       _X
        , const int &                       _Y
    )
    {
        _glCurrentContext.glLoadIdentity();

        translate_(
            _glCurrentContext
            , _BLOCK_SIZE
            , _X
            , _Y
        );

        rotate_(
            _glCurrentContext
            , _DIRECTION
        );

        scale_(
            _glCurrentContext
            , _SCALE
        );
    }

    void paintFoodForm(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const int &           _X
        , const int &           _Y
    )
    {
        const auto  FOOD_SIZE = _BLOCK_SIZE - 4;    //TODO

        const auto  X1 = -( FOOD_SIZE / 2 );
        const auto  X2 = 0;
        const auto  X3 = FOOD_SIZE / 2;
        const auto  Y1 = -( FOOD_SIZE / 2 );
        const auto  Y2 = 0;
        const auto  Y3 = FOOD_SIZE / 2;

        translate(
            _glCurrentContext
            , _BLOCK_SIZE
            , _X
            , _Y
        );

        _glCurrentContext.glBegin( fg::GL_QUADS );

        _glCurrentContext.glVertex2f( X1, Y2 );
        _glCurrentContext.glVertex2f( X2, Y3 );
        _glCurrentContext.glVertex2f( X3, Y2 );
        _glCurrentContext.glVertex2f( X2, Y1 );

        _glCurrentContext.glEnd();
    }

    void paintFood(
        fg::GLCurrentContext &      _glCurrentContext
        , const cmsnake::Stage &    _STAGE
        , const float &             _BLOCK_SIZE
    )
    {
        _glCurrentContext.glColor3f( 1, 0, 0 );

        paintFoodForm(
            _glCurrentContext
            , _BLOCK_SIZE
            , _STAGE.foodX
            , _STAGE.foodY
        );
    }

    void paintSnakeHead(
        fg::GLCurrentContext &              _glCurrentContext
        , const float &                     _BLOCK_SIZE
        , const float &                     _SNAKE_WIDTH
        , const int &                       _X
        , const int &                       _Y
        , const cmsnake::Snake::Direction & _DIRECTION
    )
    {
        const auto  X1 = -( _SNAKE_WIDTH / 2 );
        const auto  X2 = 0;
        const auto  X3 = _SNAKE_WIDTH / 2;
        const auto  Y1 = 0;
        const auto  Y2 = _SNAKE_WIDTH / 2;
        const auto  Y3 = _BLOCK_SIZE / 2;

        rotateTranslate(
            _glCurrentContext
            , _BLOCK_SIZE
            , _DIRECTION
            , _X
            , _Y
        );

        _glCurrentContext.glBegin( fg::GL_TRIANGLES );

        _glCurrentContext.glVertex2f( X1, Y1 );
        _glCurrentContext.glVertex2f( X1, Y3 );
        _glCurrentContext.glVertex2f( X2, Y2 );

        _glCurrentContext.glVertex2f( X1, Y1 );
        _glCurrentContext.glVertex2f( X2, Y2 );
        _glCurrentContext.glVertex2f( X3, Y1 );

        _glCurrentContext.glVertex2f( X2, Y2 );
        _glCurrentContext.glVertex2f( X3, Y3 );
        _glCurrentContext.glVertex2f( X3, Y1 );

        _glCurrentContext.glEnd();
    }

    void paintSnakeConnector(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const float &         _SNAKE_WIDTH
        , const int &           _X
        , const int &           _Y
    )
    {
        const auto  X1 = -( _SNAKE_WIDTH / 2 );
        const auto  X2 = 0;
        const auto  X3 = _SNAKE_WIDTH / 2;
        const auto  Y1 = -( _SNAKE_WIDTH / 2 );
        const auto  Y2 = 0;
        const auto  Y3 = _SNAKE_WIDTH / 2;

        translate(
            _glCurrentContext
            , _BLOCK_SIZE
            , _X
            , _Y
        );

        _glCurrentContext.glBegin( fg::GL_QUADS );

        _glCurrentContext.glVertex2f( X1, Y2 );
        _glCurrentContext.glVertex2f( X2, Y3 );
        _glCurrentContext.glVertex2f( X3, Y2 );
        _glCurrentContext.glVertex2f( X2, Y1 );

        _glCurrentContext.glEnd();
    }

    void paintSnakeBody(
        fg::GLCurrentContext &              _glCurrentContext
        , const float &                     _BLOCK_SIZE
        , const float &                     _SNAKE_WIDTH
        , const int &                       _X
        , const int &                       _Y
        , const cmsnake::Snake::Direction & _DIRECTION
        , const int &                       _LENGTH
    )
    {
        paintSnakeConnector(
            _glCurrentContext
            , _BLOCK_SIZE
            , _SNAKE_WIDTH
            , _X
            , _Y
        );

        const auto  X1 = -( _SNAKE_WIDTH / 2 );
        const auto  X2 = _SNAKE_WIDTH / 2;
        const auto  Y1 = -_BLOCK_SIZE;
        const auto  Y2 = 0;

        scaleRotateTranslate(
            _glCurrentContext
            , _BLOCK_SIZE
            , _LENGTH
            , _DIRECTION
            , _X
            , _Y
        );

        _glCurrentContext.glBegin( fg::GL_QUADS );

        _glCurrentContext.glVertex2f( X1, Y1 );
        _glCurrentContext.glVertex2f( X2, Y1 );
        _glCurrentContext.glVertex2f( X2, Y2 );
        _glCurrentContext.glVertex2f( X1, Y2 );

        _glCurrentContext.glEnd();
    }

    void appendLength(
        int &                               _x
        , int &                             _y
        , const int &                       _LENGTH
        , const cmsnake::Snake::Direction & _DIRECTION
    )
    {
        switch( _DIRECTION ) {
        case cmsnake::Snake::Direction::LEFT:
            _x += _LENGTH;
            break;

        case cmsnake::Snake::Direction::UP:
            _y -= _LENGTH;
            break;

        case cmsnake::Snake::Direction::RIGHT:
            _x -= _LENGTH;
            break;

        case cmsnake::Snake::Direction::DOWN:
            _y += _LENGTH;
            break;

        default:
#ifdef  DEBUG
            std::printf( "E:ありえない値\n" );
#endif  // DEBUG

            return;
            break;
        }
    }

    void paintSnakeFoods(
        fg::GLCurrentContext &                                  _glCurrentContext
        , const float &                                         _BLOCK_SIZE
        , cmsnake::Snake::Foods::const_reverse_iterator &       _it
        , const cmsnake::Snake::Foods::const_reverse_iterator & _END
        , const int &                                           _X
        , const int &                                           _Y
        , const int &                                           _BODY_START
        , const int &                                           _BODY_LENGTH
        , const cmsnake::Snake::Direction &                     _BODY_DIRECTION
    )
    {
        const auto  BODY_END = _BODY_START + _BODY_LENGTH;

        for( ; _it != _END ; ++_it ) {
            const auto &    FOOD = *_it;

            if( FOOD > BODY_END ) {
                break;
            }

            const auto  FOOD_POSITION = FOOD - _BODY_START;

            auto    x = _X;
            auto    y = _Y;
            appendLength(
                x
                , y
                , FOOD_POSITION
                , _BODY_DIRECTION
            );

            paintFoodForm(
                _glCurrentContext
                , _BLOCK_SIZE
                , x
                , y
            );
        }
    }

    void paintSnakeBodies(
        fg::GLCurrentContext &              _glCurrentContext
        , const float &                     _BLOCK_SIZE
        , const float &                     _SNAKE_WIDTH
        , const cmsnake::Snake::Bodies &    _BODIES
        , const cmsnake::Snake::Foods &     _FOODS
        , int &                             _x
        , int &                             _y
        , cmsnake::Snake::Direction &       _direction
    )
    {
        auto        foodsIt = _FOODS.rbegin();
        const auto  FOODS_END = _FOODS.rend();

        auto    totalLength = int( 0 );

        std::for_each(
            _BODIES.rbegin()
            , _BODIES.rend()
            , [
                &_glCurrentContext
                , &_BLOCK_SIZE
                , &_SNAKE_WIDTH
                , &_x
                , &_y
                , &_direction
                , &foodsIt
                , &FOODS_END
                , &totalLength
            ]
            (
                const cmsnake::Snake::Body &    _BODY
            )
            {
                const auto &    BODY_DIRECTION = _BODY.direction;
                const auto &    BODY_LENGTH = _BODY.length;

                paintSnakeBody(
                    _glCurrentContext
                    , _BLOCK_SIZE
                    , _SNAKE_WIDTH
                    , _x
                    , _y
                    , BODY_DIRECTION
                    , BODY_LENGTH
                );

                paintSnakeFoods(
                    _glCurrentContext
                    , _BLOCK_SIZE
                    , foodsIt
                    , FOODS_END
                    , _x
                    , _y
                    , totalLength
                    , BODY_LENGTH
                    , BODY_DIRECTION
                );

                totalLength += BODY_LENGTH;

                appendLength(
                    _x
                    , _y
                    , BODY_LENGTH
                    , BODY_DIRECTION
                );

                _direction = BODY_DIRECTION;
            }
        );
    }

    void paintSnakeTail(
        fg::GLCurrentContext &              _glCurrentContext
        , const float &                     _BLOCK_SIZE
        , const float &                     _SNAKE_WIDTH
        , const int &                       _X
        , const int &                       _Y
        , const cmsnake::Snake::Direction & _DIRECTION
    )
    {
        paintSnakeConnector(
            _glCurrentContext
            , _BLOCK_SIZE
            , _SNAKE_WIDTH
            , _X
            , _Y
        );

        const auto  X1 = -( _SNAKE_WIDTH / 2 );
        const auto  X2 = 0;
        const auto  X3 =  _SNAKE_WIDTH / 2;
        const auto  Y1 = -( _BLOCK_SIZE / 2 );
        const auto  Y2 = -( _SNAKE_WIDTH / 2 );
        const auto  Y3 = 0;

        rotateTranslate(
            _glCurrentContext
            , _BLOCK_SIZE
            , _DIRECTION
            , _X
            , _Y
        );

        _glCurrentContext.glBegin( fg::GL_TRIANGLES );

        _glCurrentContext.glVertex2f( X1, Y2 );
        _glCurrentContext.glVertex2f( X1, Y3 );
        _glCurrentContext.glVertex2f( X2, Y1 );

        _glCurrentContext.glVertex2f( X1, Y3 );
        _glCurrentContext.glVertex2f( X3, Y3 );
        _glCurrentContext.glVertex2f( X2, Y1 );

        _glCurrentContext.glVertex2f( X2, Y1 );
        _glCurrentContext.glVertex2f( X3, Y3 );
        _glCurrentContext.glVertex2f( X3, Y2 );

        _glCurrentContext.glEnd();
    }

    void paintSnake(
        fg::GLCurrentContext &  _glCurrentContext
        , cmsnake::Stage &      _stage
        , const float &         _BLOCK_SIZE
    )
    {
        const auto  SNAKE_WIDTH = _BLOCK_SIZE / 2;  //TODO

        auto    snake = cmsnake::Snake();
        cmsnake::getSnake(
            snake
            , _stage
        );

        _glCurrentContext.glColor3f( 0, 1, 0 );

        auto    x = snake.x;
        auto    y = snake.y;
        auto    direction = snake.direction;

        paintSnakeHead(
            _glCurrentContext
            , _BLOCK_SIZE
            , SNAKE_WIDTH
            , x
            , y
            , direction
        );

        paintSnakeBodies(
            _glCurrentContext
            , _BLOCK_SIZE
            , SNAKE_WIDTH
            , snake.bodies
            , snake.foods
            , x
            , y
            , direction
        );

        paintSnakeTail(
            _glCurrentContext
            , _BLOCK_SIZE
            , SNAKE_WIDTH
            , x
            , y
            , direction
        );
    }

    void paintWallBlock(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const int &           _X
        , const int &           _Y
    )
    {
        //TODO
        const auto  X = _BLOCK_SIZE * _X;
        const auto  X1 = X + 1;
        const auto  X2 = X + _BLOCK_SIZE - 1;
        const auto  Y = _BLOCK_SIZE * _Y;
        const auto  Y1 = Y + 1;
        const auto  Y2 = Y + _BLOCK_SIZE - 1;

        _glCurrentContext.glVertex2f( X1, Y1 );
        _glCurrentContext.glVertex2f( X2, Y1 );
        _glCurrentContext.glVertex2f( X2, Y2 );
        _glCurrentContext.glVertex2f( X1, Y2 );
    }

    void paintHorizontalWall(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const int &           _WIDTH
        , const int &           _Y
    )
    {
        for( auto x = 0 ; x < _WIDTH ; x++ ) {
            paintWallBlock(
                _glCurrentContext
                , _BLOCK_SIZE
                , x
                , _Y
            );
        }
    }

    void paintVerticalWall(
        fg::GLCurrentContext &  _glCurrentContext
        , const float &         _BLOCK_SIZE
        , const int &           _X
        , const int &           _HEIGHT
    )
    {
        for( auto y = 0 ; y < _HEIGHT ; y++ ) {
            paintWallBlock(
                _glCurrentContext
                , _BLOCK_SIZE
                , _X
                , y
            );
        }
    }

    void paintWall(
        fg::GLCurrentContext &      _glCurrentContext
        , const cmsnake::Stage &    _STAGE
        , const float &             _BLOCK_SIZE
    )
    {
        const auto  STAGE_WIDTH = cmsnake::getWidth( _STAGE );
        const auto  STAGE_HEIGHT = cmsnake::getHeight( _STAGE );

        _glCurrentContext.glColor3f( 1, 1, 1 );

        _glCurrentContext.glLoadIdentity();

        _glCurrentContext.glBegin( fg::GL_QUADS );

        paintHorizontalWall(
            _glCurrentContext
            , _BLOCK_SIZE
            , STAGE_WIDTH
            , 0
        );

        paintHorizontalWall(
            _glCurrentContext
            , _BLOCK_SIZE
            , STAGE_WIDTH
            , STAGE_HEIGHT - 1
        );

        paintVerticalWall(
            _glCurrentContext
            , _BLOCK_SIZE
            , 0
            , STAGE_HEIGHT
        );

        paintVerticalWall(
            _glCurrentContext
            , _BLOCK_SIZE
            , STAGE_WIDTH - 1
            , STAGE_HEIGHT
        );

        _glCurrentContext.glEnd();
    }
}

namespace cmsnake {
    void paint(
        fg::GLCurrentContext &  _glCurrentContext
        , GameData &            _gameData
    )
    {
        _glCurrentContext.glEnable( fg::GL_POLYGON_SMOOTH );
        _glCurrentContext.glHint(
            fg::GL_POLYGON_SMOOTH_HINT
            , fg::GL_NICEST
        );

        _glCurrentContext.glMatrixMode( fg::GL_PROJECTION );
        _glCurrentContext.glLoadIdentity();
        _glCurrentContext.glOrtho( 0, 640, 0, 480, -1, 1 );

        _glCurrentContext.glMatrixMode( fg::GL_MODELVIEW );
        _glCurrentContext.glClearColor( 0, 0, 0, 0 );
        _glCurrentContext.glClear( fg::GL_COLOR_BUFFER_BIT );

        auto &  stage = _gameData.stage;

        const auto  BLOCK_SIZE = static_cast< float >( cmsnake::getBlockSize( stage ) );

        paintFood(
            _glCurrentContext
            , stage
            , BLOCK_SIZE
        );

        paintSnake(
            _glCurrentContext
            , stage
            , BLOCK_SIZE
        );

        paintWall(
            _glCurrentContext
            , stage
            , BLOCK_SIZE
        );
    }
}
