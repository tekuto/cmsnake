﻿#include "fg/util/test.h"
#include "cmsnake/gamedata/timer.h"

#include <mutex>
#include <condition_variable>
#include <chrono>

TEST(
    TimerTest
    , Timer
)
{
    cmsnake::TimerData  timerData;

    cmsnake::initialize( timerData );

    ASSERT_FALSE( cmsnake::timer( timerData ) );

    {
        std::mutex              mutex;
        std::condition_variable condition;

        auto    lock = std::unique_lock< std::mutex >( mutex );

        condition.wait_for(
            lock
            , std::chrono::milliseconds( 70 )
        );
    }

    ASSERT_FALSE( cmsnake::timer( timerData ) );

    {
        std::mutex              mutex;
        std::condition_variable condition;

        auto    lock = std::unique_lock< std::mutex >( mutex );

        condition.wait_for(
            lock
            , std::chrono::milliseconds( 70 )
        );
    }

    ASSERT_TRUE( cmsnake::timer( timerData ) );
    ASSERT_FALSE( cmsnake::timer( timerData ) );
}
