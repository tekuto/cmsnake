﻿#include "cmsnake/gamedata/stage/stage.h"
#include "cmsnake/gamedata/stage/snake.h"

#include <random>
#include <mutex>
#include <utility>

namespace {
    enum
    {
        STAGE_WIDTH = 32,
        STAGE_HEIGHT = 24,

        BLOCK_SIZE = 20,
    };

    int getRandom(
        cmsnake::Stage &    _stage
        , const int &       _MIN
        , const int &       _MAX
    )
    {
        auto    random = _stage.distribution( _stage.randomEngine );

        return random * ( _MAX - _MIN ) + _MIN;
    }

    void setFood(
        cmsnake::Stage &    _stage
    )
    {
        _stage.foodX = getRandom(
            _stage
            , 1
            , STAGE_WIDTH - 1
        );

        _stage.foodY = getRandom(
            _stage
            , 1
            , STAGE_HEIGHT - 1
        );
    }

    bool checkCrachedToWall(
        const int &     _POSITION
        , const int &   _WALL
    )
    {
        if( _POSITION <= 0 ) {
            return true;
        }

        if( _POSITION >= _WALL - 1 ) {
            return true;
        }

        return false;
    }

    bool isCrashedToWall(
        const cmsnake::Snake &  _SNAKE
    )
    {
        if( checkCrachedToWall(
            _SNAKE.x
            , STAGE_WIDTH
        ) == true ) {
            return true;
        }

        if( checkCrachedToWall(
            _SNAKE.y
            , STAGE_HEIGHT
        ) == true ) {
            return true;
        }

        return false;
    }
}

namespace cmsnake {
    void initialize(
        Stage & _this
    )
    {
        initialize(
            _this.snake
            , _this
        );

        std::random_device  randomDevice;

        auto    randomEngine = std::mt19937( randomDevice() );
        auto    distribution = std::uniform_real_distribution<>( 0.0, 1.0 );

        _this.randomEngine = std::move( randomEngine );
        _this.distribution = std::move( distribution );

        setFood( _this );
    }

    int getWidth(
        const Stage &
    )
    {
        return STAGE_WIDTH;
    }

    int getHeight(
        const Stage &
    )
    {
        return STAGE_HEIGHT;
    }

    int getBlockSize(
        const Stage &
    )
    {
        return BLOCK_SIZE;
    }

    void getSnake(
        Snake &     _snake
        , Stage &   _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        _snake = _this.snake;
    }

    bool setSnakeDirection(
        Stage &                     _this
        , const Snake::Direction &  _DIRECTION
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        return setDirection(
            _this.snake
            , _DIRECTION
        );
    }

    void forwardSnake(
        Stage & _this
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        forward( _this.snake );

        if( _this.foodX == _this.snake.x && _this.foodY == _this.snake.y ) {
            eat( _this.snake );
            setFood( _this );
        }
    }

    bool isCrashed(
        Stage & _this
    )
    {
        const auto &    SNAKE = _this.snake;

        auto    lock = std::unique_lock< std::mutex >( _this.mutex );

        if( isCrashedToWall( SNAKE ) == true ) {
            return true;
        }

        if( isCrashed( SNAKE ) == true ) {
            return true;
        }

        return false;
    }
}
