﻿#include "fg/util/test.h"
#include "cmsnake/gamedata/stage/snake.h"

void testForwardLeft(
    int _x
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::LEFT,
        _x,
        10,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    cmsnake::forward( snake );

    ASSERT_EQ( _x - 1, snake.x );
    ASSERT_EQ( 10, snake.y );
}

TEST(
    SnakeTest
    , Forward_Left
)
{
    ASSERT_NO_FATAL_FAILURE( testForwardLeft( 10 ) );
    ASSERT_NO_FATAL_FAILURE( testForwardLeft( 20 ) );
}

void testForwardRight(
    int _x
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::RIGHT,
        _x,
        10,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    cmsnake::forward( snake );

    ASSERT_EQ( _x + 1, snake.x );
    ASSERT_EQ( 10, snake.y );
}

TEST(
    SnakeTest
    , Forward_Right
)
{
    ASSERT_NO_FATAL_FAILURE( testForwardRight( 10 ) );
    ASSERT_NO_FATAL_FAILURE( testForwardRight( 20 ) );
}

void testForwardDown(
    int _y
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::DOWN,
        10,
        _y,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    cmsnake::forward( snake );

    ASSERT_EQ( 10, snake.x );
    ASSERT_EQ( _y - 1, snake.y );
}

TEST(
    SnakeTest
    , Forward_Down
)
{
    ASSERT_NO_FATAL_FAILURE( testForwardDown( 10 ) );
    ASSERT_NO_FATAL_FAILURE( testForwardDown( 20 ) );
}

void testForwardUp(
    int _y
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::UP,
        10,
        _y,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    cmsnake::forward( snake );

    ASSERT_EQ( 10, snake.x );
    ASSERT_EQ( _y + 1, snake.y );
}

TEST(
    SnakeTest
    , Forward_Up
)
{
    ASSERT_NO_FATAL_FAILURE( testForwardUp( 10 ) );
    ASSERT_NO_FATAL_FAILURE( testForwardUp( 20 ) );
}

void testForwardBodies(
    const cmsnake::Snake::Direction &   _DIRECTION
    , const cmsnake::Snake::Bodies &    _BODIES
    , const cmsnake::Snake::Bodies &    _FORWARDED_BODIES
)
{
    auto    snake = cmsnake::Snake{
        _DIRECTION,
        0,
        0,
        _BODIES,
    };

    cmsnake::forward( snake );

    const auto &    BODIES = snake.bodies;
    const auto      BODIES_SIZE = BODIES.size();
    ASSERT_EQ( _FORWARDED_BODIES.size(), BODIES_SIZE );

    for( auto i = size_t( 0 ) ; i < BODIES_SIZE ; i++ ) {
        const auto &    BODY = BODIES[ i ];
        const auto &    FORWARDED_BODY = _FORWARDED_BODIES[ i ];

        ASSERT_EQ( FORWARDED_BODY.direction, BODY.direction );
        ASSERT_EQ( FORWARDED_BODY.length, BODY.length );
    }
}

TEST(
    SnakeTest
    , Forward_Bodies
)
{
    ASSERT_NO_FATAL_FAILURE(
        testForwardBodies(
            cmsnake::Snake::Direction::UP
            , {
                {
                    cmsnake::Snake::Direction::LEFT,
                    10,
                },
            }
            , {
                {
                    cmsnake::Snake::Direction::LEFT,
                    9,
                },
                {
                    cmsnake::Snake::Direction::UP,
                    1,
                },
            }
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testForwardBodies(
            cmsnake::Snake::Direction::LEFT
            , {
                {
                    cmsnake::Snake::Direction::UP,
                    20,
                },
            }
            , {
                {
                    cmsnake::Snake::Direction::UP,
                    19,
                },
                {
                    cmsnake::Snake::Direction::LEFT,
                    1,
                },
            }
        )
    );

    ASSERT_NO_FATAL_FAILURE(
        testForwardBodies(
            cmsnake::Snake::Direction::LEFT
            , {
                {
                    cmsnake::Snake::Direction::UP,
                    10,
                },
                {
                    cmsnake::Snake::Direction::LEFT,
                    10,
                },
            }
            , {
                {
                    cmsnake::Snake::Direction::UP,
                    9,
                },
                {
                    cmsnake::Snake::Direction::LEFT,
                    11,
                },
            }
        )
    );

    ASSERT_NO_FATAL_FAILURE(
        testForwardBodies(
            cmsnake::Snake::Direction::LEFT
            , {
                {
                    cmsnake::Snake::Direction::UP,
                    0,
                },
                {
                    cmsnake::Snake::Direction::LEFT,
                    10,
                },
            }
            , {
                {
                    cmsnake::Snake::Direction::LEFT,
                    10,
                },
            }
        )
    );
}

TEST(
    SnakeTest
    , Eat
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::LEFT,
        0,
        0,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    const auto &    FOODS = snake.foods;

    cmsnake::eat( snake );

    ASSERT_EQ( 1, FOODS.size() );
    ASSERT_EQ( 0, FOODS[ 0 ] );

    cmsnake::eat( snake );

    ASSERT_EQ( 2, FOODS.size() );
    ASSERT_EQ( 0, FOODS[ 0 ] );
    ASSERT_EQ( 0, FOODS[ 1 ] );
}

TEST(
    SnakeTest
    , Forward_MoveFoods
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::LEFT,
        0,
        0,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                10,
            },
        },
    };

    const auto &    FOODS = snake.foods;

    cmsnake::eat( snake );
    cmsnake::forward( snake );

    ASSERT_EQ( 1, FOODS.size() );
    ASSERT_EQ( 1, FOODS[ 0 ] );

    cmsnake::eat( snake );
    cmsnake::forward( snake );

    ASSERT_EQ( 2, FOODS.size() );
    ASSERT_EQ( 2, FOODS[ 0 ] );
    ASSERT_EQ( 1, FOODS[ 1 ] );
}

TEST(
    SnakeTest
    , Forward_ExtendBody
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::LEFT,
        0,
        0,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    cmsnake::eat( snake );
    cmsnake::forward( snake );

    ASSERT_EQ( 0, snake.foods.size() );

    const auto &    BODIES = snake.bodies;
    ASSERT_EQ( 1, BODIES.size() );
    ASSERT_EQ( 1, BODIES[ 0 ].length );
}

TEST(
    SnakeTest
    , Forward_ExtendBodyCurve
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::LEFT,
        0,
        0,
        {
            {
                cmsnake::Snake::Direction::LEFT,
                0,
            },
        },
    };

    cmsnake::eat( snake );
    cmsnake::setDirection(
        snake
        , cmsnake::Snake::Direction::UP
    );
    cmsnake::forward( snake );

    cmsnake::eat( snake );
    cmsnake::setDirection(
        snake
        , cmsnake::Snake::Direction::LEFT
    );
    cmsnake::forward( snake );

    cmsnake::eat( snake );
    cmsnake::setDirection(
        snake
        , cmsnake::Snake::Direction::UP
    );
    cmsnake::forward( snake );

    const auto &    FOODS = snake.foods;
    ASSERT_EQ( 1, FOODS.size() );
    ASSERT_EQ( 1, FOODS[ 0 ] );

    const auto &    BODIES = snake.bodies;
    ASSERT_EQ( 3, BODIES.size() );
    ASSERT_EQ( 0, BODIES[ 0 ].length );
    ASSERT_EQ( cmsnake::Snake::Direction::UP, BODIES[ 0 ].direction );
    ASSERT_EQ( 1, BODIES[ 1 ].length );
    ASSERT_EQ( cmsnake::Snake::Direction::LEFT, BODIES[ 1 ].direction );
    ASSERT_EQ( 1, BODIES[ 2 ].length );
    ASSERT_EQ( cmsnake::Snake::Direction::UP, BODIES[ 2 ].direction );
}

void testIsCrashed(
    const cmsnake::Snake::Bodies &  _BODIES
    , bool                          _CRASHED
)
{
    auto    snake = cmsnake::Snake{
        cmsnake::Snake::Direction::LEFT,
        0,
        0,
        _BODIES,
    };

    ASSERT_EQ( _CRASHED, isCrashed( snake ) );
}

TEST(
    SnakeTest
    , IsCrashed
)
{
    ASSERT_NO_FATAL_FAILURE(
        testIsCrashed(
            {
                {
                    cmsnake::Snake::Direction::DOWN,
                    3,
                },
                {
                    cmsnake::Snake::Direction::RIGHT,
                    3,
                },
                {
                    cmsnake::Snake::Direction::UP,
                    3,
                },
                {
                    cmsnake::Snake::Direction::LEFT,
                    3,
                },
            }
            , true
        )
    );
    ASSERT_NO_FATAL_FAILURE(
        testIsCrashed(
            {
                {
                    cmsnake::Snake::Direction::DOWN,
                    2,
                },
                {
                    cmsnake::Snake::Direction::RIGHT,
                    3,
                },
                {
                    cmsnake::Snake::Direction::UP,
                    3,
                },
                {
                    cmsnake::Snake::Direction::LEFT,
                    3,
                },
            }
            , false
        )
    );
}
