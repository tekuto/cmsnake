﻿#include "cmsnake/gamedata/stage/snake.h"
#include "cmsnake/gamedata/stage/stage.h"

#include <algorithm>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    void forwardHead(
        cmsnake::Snake &    _snake
    )
    {
        const auto &    HEAD_DIRECTION = _snake.direction;
        switch( HEAD_DIRECTION ) {
        case cmsnake::Snake::Direction::LEFT:
            _snake.x--;
            break;

        case cmsnake::Snake::Direction::RIGHT:
            _snake.x++;
            break;

        case cmsnake::Snake::Direction::DOWN:
            _snake.y--;
            break;

        case cmsnake::Snake::Direction::UP:
            _snake.y++;
            break;

        default:
#ifdef  DEBUG
            std::printf( "E:ありえない値\n" );
#endif  // DEBUG

            return;
            break;
        }
    }

    void forwardFoods(
        cmsnake::Snake::Foods & _foods
    )
    {
        for( auto & food : _foods ) {
            food++;
        }
    }

    void addBodyWhenChangeDirection(
        cmsnake::Snake::Bodies &            _bodies
        , const cmsnake::Snake::Direction & _HEAD_DIRECTION
    )
    {
        if( _bodies.rbegin()->direction == _HEAD_DIRECTION ) {
            return;
        }

        _bodies.push_back(
            {
                _HEAD_DIRECTION,
                0,
            }
        );
    }

    void forwardBodyHead(
        cmsnake::Snake::Bodies &    _bodies
    )
    {
        _bodies.rbegin()->length++;
    }

    void forwardBodyTail(
        cmsnake::Snake::Bodies &    _bodies
    )
    {
        _bodies[ 0 ].length--;
    }

    void removeBodyWhenEndCurve(
        cmsnake::Snake::Bodies &    _bodies
    )
    {
        if( _bodies[ 0 ].length > 0 ) {
            return;
        }

        _bodies.erase( _bodies.begin() );

        return;
    }

    int getBodyLength(
        const cmsnake::Snake::Bodies &  _BODIES
    )
    {
        auto    bodyLength = 0;
        for( const auto & BODY : _BODIES ) {
            bodyLength += BODY.length;
        }

        return bodyLength;
    }

    void extendBodyByFood(
        cmsnake::Snake::Bodies &    _bodies
        , cmsnake::Snake::Foods &   _foods
    )
    {
        if( _foods.size() <= 0 ) {
            return;
        }

        const auto  BODY_LENGTH = getBodyLength( _bodies );
        if( _foods[ 0 ] > BODY_LENGTH ) {
            _foods.erase( _foods.begin() );
            _bodies[ 0 ].length++;
        }
    }

    void getBodyTail(
        auto &                          _x
        , auto &                        _y
        , const cmsnake::Snake::Body &  _BODY
    )
    {
        const auto &    BODY_LENGTH = _BODY.length;
        switch( _BODY.direction ) {
        case cmsnake::Snake::Direction::LEFT:
            _x += BODY_LENGTH;
            break;

        case cmsnake::Snake::Direction::UP:
            _y -= BODY_LENGTH;
            break;

        case cmsnake::Snake::Direction::RIGHT:
            _x -= BODY_LENGTH;
            break;

        case cmsnake::Snake::Direction::DOWN:
            _y += BODY_LENGTH;
            break;

        default:
#ifdef  DEBUG
            std::printf( "E:ありえない値\n" );
#endif  // DEBUG

            return;
            break;
        }
    }

    bool checkCrashPosition(
        const int &     _POSITION
        , const int &   _START
        , const int &   _END
    )
    {
        if( _POSITION < _START ) {
            return false;
        }

        if( _POSITION > _END ) {
            return false;
        }

        return true;
    }

    bool checkCrash(
        const int &     _HEAD
        , const int &   _BODY
        , const int &   _HEAD_POSITION
        , const int &   _BODY_START
        , const int &   _BODY_END
    )
    {
        if( _HEAD != _BODY ) {
            return false;
        }

        // STARTがENDより大きい場合もあるため、双方向チェックする
        if( checkCrashPosition(
            _HEAD_POSITION
            , _BODY_START
            , _BODY_END
        ) == false && checkCrashPosition(
            _HEAD_POSITION
            , _BODY_END
            , _BODY_START
        ) == false ) {
            return false;
        }

        return true;
    }

    bool checkCrash(
        const int &     _HEAD_X
        , const int &   _HEAD_Y
        , const int &   _BODY_HEAD_X
        , const int &   _BODY_HEAD_Y
        , const int &   _BODY_TAIL_X
        , const int &   _BODY_TAIL_Y
    )
    {
        // X軸が一致する場合の、Y軸についての衝突チェック
        if( checkCrash(
            _HEAD_X
            , _BODY_HEAD_X
            , _HEAD_Y
            , _BODY_HEAD_Y
            , _BODY_TAIL_Y
        ) == true ) {
            return true;
        }

        // Y軸が一致する場合の、X軸についての衝突チェック
        if( checkCrash(
            _HEAD_Y
            , _BODY_HEAD_Y
            , _HEAD_X
            , _BODY_HEAD_X
            , _BODY_TAIL_X
        ) == true ) {
            return true;
        }

        return false;
    }
}

namespace cmsnake {
    void initialize(
        Snake &         _this
        , const Stage & _STAGE
    )
    {
        _this.direction = Snake::Direction::LEFT;
        _this.x = getWidth( _STAGE ) / 2;
        _this.y = getHeight( _STAGE ) / 2;

        _this.bodies.push_back(
            Snake::Body{
                _this.direction,
                0,
            }
        );
    }

    bool setDirection(
        Snake &                     _this
        , const Snake::Direction &  _DIRECTION
    )
    {
        const auto &    CURRENT_DIRECTION = _this.bodies.rbegin()->direction;

        switch( CURRENT_DIRECTION ) {
        case Snake::Direction::UP:
            if( _DIRECTION == Snake::Direction::DOWN ) {
                return false;
            }
            break;

        case Snake::Direction::LEFT:
            if( _DIRECTION == Snake::Direction::RIGHT ) {
                return false;
            }
            break;

        case Snake::Direction::DOWN:
            if( _DIRECTION == Snake::Direction::UP ) {
                return false;
            }
            break;

        case Snake::Direction::RIGHT:
            if( _DIRECTION == Snake::Direction::LEFT ) {
                return false;
            }
            break;

        default:
#ifdef  DEBUG
            std::printf( "E:ありえない値\n" );
#endif  // DEBUG

            return false;
            break;
        }

        _this.direction = _DIRECTION;

        return true;
    }

    void forward(
        Snake & _this
    )
    {
        forwardHead( _this );

        auto &  foods = _this.foods;

        forwardFoods( foods );

        auto &  bodies = _this.bodies;

        extendBodyByFood(
            bodies
            , foods
        );

        addBodyWhenChangeDirection(
            bodies
            , _this.direction
        );

        forwardBodyHead( bodies );

        removeBodyWhenEndCurve( bodies );

        forwardBodyTail( bodies );
    }

    void eat(
        Snake & _this
    )
    {
        _this.foods.push_back( 0 );
    }

    bool isCrashed(
        const Snake &   _THIS
    )
    {
        const auto &    HEAD_X = _THIS.x;
        const auto &    HEAD_Y = _THIS.y;

        const auto &    BODIES = _THIS.bodies;

        auto    it = BODIES.rbegin();

        auto    bodyTailX = HEAD_X;
        auto    bodyTailY = HEAD_Y;
        getBodyTail(
            bodyTailX
            , bodyTailY
            , *it
        );
        it++;

        const auto  END = BODIES.rend();

        if( std::find_if(
            it
            , END
            , [
                &HEAD_X
                , &HEAD_Y
                , &bodyTailX
                , &bodyTailY
            ]
            (
                const Snake::Body & _BODY
            )
            {
                const auto  BODY_HEAD_X = bodyTailX;
                const auto  BODY_HEAD_Y = bodyTailY;

                getBodyTail(
                    bodyTailX
                    , bodyTailY
                    , _BODY
                );

                if( checkCrash(
                    HEAD_X
                    , HEAD_Y
                    , BODY_HEAD_X
                    , BODY_HEAD_Y
                    , bodyTailX
                    , bodyTailY
                ) == false ) {
                    return false;
                }

                return true;
            }
        ) == END ) {
            return false;
        }

        return true;
    }
}
