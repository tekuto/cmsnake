﻿#include "cmsnake/gamedata/timer.h"

#include <chrono>
#include <mutex>

namespace {
    const auto  TIMER_TIME_USECONDS = 100000;
    const auto  WAIT_TIME_USECONDS = 10000;
}

namespace cmsnake {
    void initialize(
        TimerData & _this
    )
    {
        _this.started = false;
    }

    bool timer(
        TimerData & _this
    )
    {
        const auto  CURRENT_TIME = std::chrono::steady_clock::now();

        if( _this.started == false ) {
            _this.started = true;
            _this.startTime = CURRENT_TIME;

            return false;
        }

        const auto  DIFF_USECONDS = std::chrono::duration_cast< std::chrono::microseconds >( CURRENT_TIME - _this.startTime );
        if( DIFF_USECONDS.count() < TIMER_TIME_USECONDS ) {
            auto    lock = std::unique_lock< std::mutex >( _this.mutex );

            _this.condition.wait_for(
                lock
                , std::chrono::microseconds( WAIT_TIME_USECONDS )
            );

            return false;
        }

        _this.startTime = CURRENT_TIME;

        return true;
    }
}
