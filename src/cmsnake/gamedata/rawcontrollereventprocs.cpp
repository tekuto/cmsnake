﻿#include "cmsnake/gamedata/rawcontrollereventprocs.h"
#include "cmsnake/gamedata/gamedata.h"
#include "cmsnake/gamedata/stage/snake.h"
#include "fg/core/game/context.h"
#include "fg/controller/raw/event.h"
#include "fg/controller/raw/actionbuffer.h"
#include "fg/controller/raw/action.h"
#include "fg/controller/raw/axisaction.h"
#include "fg/controller/raw/axis.h"
#include "fg/window/window.h"

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const fg::RawControllerAxisAction * getAxisAction(
        const fg::RawControllerActionBuffer &   _ACTION_BUFFER
    )
    {
        for( auto i = _ACTION_BUFFER.getSize() ; i > 0 ; i-- ) {
            const auto &    ACTION = _ACTION_BUFFER.getAction( i - 1 );

            const auto  AXIS_ACTION_PTR = ACTION.getAxisAction();
            if( AXIS_ACTION_PTR == nullptr ) {
                continue;
            }

            return AXIS_ACTION_PTR;
        }

        return nullptr;
    }
}

namespace cmsnake {
    void rawControllerEventProc(
        fg::RawControllerEvent<> &  _event
    )
    {
        auto &  eventData = _event.getEventData();

        auto &  gameContext = _event.getGameContext();
        auto &  gameData = gameContext.getData< GameData >();

        const auto &    ACTION_BUFFER = eventData.getActionBuffer();

        const auto  AXIS_ACTION_PTR = getAxisAction( ACTION_BUFFER );
        if( AXIS_ACTION_PTR == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:軸アクションではない\n" );
#endif  // DEBUG

            return;
        }
        const auto &    AXIS_ACTION = *AXIS_ACTION_PTR;

        const auto  INDEX = AXIS_ACTION.getAxis().getIndex();
        const auto  VALUE = AXIS_ACTION.getValue();

        auto    snakeDirection = Snake::Direction( Snake::Direction::NEUTRAL );

        if( INDEX == 0 ) {
            if( VALUE < 0 ) {
                snakeDirection = Snake::Direction::LEFT;
            } else if( VALUE > 0 ) {
                snakeDirection = Snake::Direction::RIGHT;
            }
        } else if( INDEX == 1 ) {
            if( VALUE < 0 ) {
                snakeDirection = Snake::Direction::UP;
            } else if( VALUE > 0 ) {
                snakeDirection = Snake::Direction::DOWN;
            }
        }

        if( snakeDirection == Snake::Direction::NEUTRAL ) {
            return;
        }

        if( gameData.setSnakeDirection( snakeDirection ) == false ) {
            return;
        }

        fg::Window::getMainWindow( gameContext ).repaint();
    }
}
