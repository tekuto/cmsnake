﻿#include "cmsnake/gamedata/windoweventprocs.h"
#include "cmsnake/gamedata/gamedata.h"
#include "cmsnake/gamedata/paint.h"
#include "fg/core/game/context.h"
#include "fg/window/drawevent.h"
#include "fg/gl/currentcontext.h"

namespace cmsnake {
    void windowDrawEventProc(
        fg::WindowDrawEvent<> & _event
    )
    {
        auto &  gameData = _event.getGameContext().getData< GameData >();

        auto    glCurrentContextUnique = fg::GLCurrentContext::create( *( gameData.glContextUnique ) );

        paint(
            *glCurrentContextUnique
            , gameData
        );
    }
}
