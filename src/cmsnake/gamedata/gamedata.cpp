﻿#include "cmsnake/gamedata/gamedata.h"
#include "cmsnake/gamedata/stage/stage.h"
#include "cmsnake/gamedata/timer.h"
#include "fg/core/game/context.h"
#include "fg/state/stack.h"
#include "fg/task/manager.h"
#include "fg/task/simpletask.h"
#include "fg/task/task.h"
#include "fg/gl/context.h"
#include "fg/window/window.h"
#include "fg/window/drawevent.h"
#include "fg/controller/raw/event.h"

#include <utility>

namespace {
    bool timerMain(
        fg::SimpleTask<> &  _task
    )
    {
        auto &  gameContext = _task.getGameContext();
        auto &  gameData = gameContext.getData< cmsnake::GameData >();

        if( cmsnake::timer( gameData.timerData ) == false ) {
            return true;
        }

        auto &  stage = gameData.stage;

        cmsnake::forwardSnake( stage );

        fg::Window::getMainWindow( gameContext ).repaint();

        if( cmsnake::isCrashed( stage ) == true ) {
            return false;
        }

        return true;
    }

    void timerTaskProc(
        fg::SimpleTask<> &  _task
    )
    {
        if( timerMain( _task ) == false ) {
            return;
        }

        _task.reexecute();
    }
}

namespace cmsnake {
    GameData::GameData(
        fg::GameContext &   _gameContext
    )
        : glContextUnique( fg::GLContext::create( fg::Window::getMainWindow( _gameContext ) ) )
        , stateStackUnique( fg::StateStack::create( _gameContext ) )
        , taskManagerUnique( fg::TaskManager::create( _gameContext ) )
    {
        initialize( this->stage );
        initialize( this->timerData );

        this->taskManagerUnique->execute( timerTaskProc );
    }

    void GameData::destroy(
        GameData *  _this
    )
    {
        delete _this;
    }

    bool GameData::setSnakeDirection(
        const Snake::Direction &    _DIRECTION
    )
    {
        auto &  stage = this->stage;

        if( isCrashed( stage ) == true ) {
            return false;
        }

        return cmsnake::setSnakeDirection(
            stage
            , _DIRECTION
        );
    }
}
