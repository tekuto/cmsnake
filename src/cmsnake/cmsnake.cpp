﻿#include "fg/util/export.h"
#include "cmsnake/gamedata/gamedata.h"
#include "cmsnake/gamedata/windoweventprocs.h"
#include "cmsnake/gamedata/rawcontrollereventprocs.h"
#include "fg/core/game/context.h"
#include "fg/state/stack.h"
#include "fg/state/state.h"
#include "fg/window/drawevent.h"
#include "fg/controller/raw/event.h"

#include <utility>

namespace {
    enum {
        INVALID = 0,

        WINDOW_DRAW_EVENT,
        RAW_CONTROLLER_EVENT,
    };

    void initializeState(
        fg::State & _state
    )
    {
        _state.setEventProc(
            WINDOW_DRAW_EVENT
            , cmsnake::windowDrawEventProc
        );

        _state.setEventProc(
            RAW_CONTROLLER_EVENT
            , cmsnake::rawControllerEventProc
        );
    }
}

namespace cmsnake {
    FG_FUNCTION_VOID(
        initialize(
            fg::GameContext &
        )
    )

    void initialize(
        fg::GameContext &   _gameContext
    )
    {
        auto    gameDataUnique = GameData::create( _gameContext );
        auto &  gameData = *gameDataUnique;

        _gameContext.setData(
            gameDataUnique.release()
            , GameData::destroy
        );

        auto &  stateStack = *( gameData.stateStackUnique );

        gameData.windowDrawEventRegisterManagerUnique = fg::WindowDrawEventRegisterManager::create(
            _gameContext
            , stateStack
            , WINDOW_DRAW_EVENT
        );

        gameData.rawControllerEventRegisterManagerUnique = fg::RawControllerEventRegisterManager::create(
            _gameContext
            , stateStack
            , RAW_CONTROLLER_EVENT
        );

        stateStack.push( initializeState );
    }
}
